#!/usr/bin/env python3
#
# SPDX-License-Identifier: MIT
#
# This file is formatted with Python Black
#

from pathlib import Path
import yaml
import os
import sys

try:
    files = [Path(sys.argv[1])]
except IndexError:
    files = Path(".").glob("**/*.recording")


for filename in files:
    print(f"Processing {filename}")
    dirname = filename.parent.name
    vendor, product, name = dirname.split("-", maxsplit=2)

    yml = yaml.safe_load(open(filename))
    ndevices = yml["ndevices"]
    if ndevices == 1:
        continue

    print(f"Splitting {filename} with {ndevices}")

    devices = yml["devices"]
    the_device = {}
    for d in devices:
        devname = d["evdev"]["name"].replace("/", "")
        if devname == name:
            # We know our device now, let's strip the lines from all other
            # devices
            devnode = d["node"]
            print(f"Found matching device for this directory: {devnode}")

            out_lines = []
            skip = False

            with open(filename) as in_fd:
                for line in in_fd:
                    # Note: line has trailing \n
                    if line.startswith(f"ndevices: {ndevices}"):
                        out_lines.append("ndevices: 1\n")
                        continue

                    if line.startswith("- node: "):
                        skip = not line.startswith(f"- node: {devnode}")

                    if not skip:
                        out_lines.append(line)
                    if line.startswith("# From "):
                        out_lines.append(
                            f"# Original recording had ndevices: {ndevices}\n"
                        )

            with open(f"{filename}.tmp", "w+") as out_fd:
                out_fd.write("".join(out_lines))

            os.rename(f"{filename}.tmp", filename)
            break
    else:
        print(f"Unable to match {filename} with a device")
