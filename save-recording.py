#!/usr/bin/env python3

# SPDX-License-Identifier: MIT
#
# This file is formatted with Python Black
#

from pathlib import Path
from typing import List, Tuple

import argparse
import subprocess
import yaml


argparse = argparse.ArgumentParser(
    description="Saves and git commits a libinput record file."
)
argparse.add_argument("issue", type=int, help="libinput issue number this recording came from")
argparse.add_argument("recording", type=str, help="Path to the file")

ns = argparse.parse_args()
issue_id = ns.issue
issue = f"https://gitlab.freedesktop.org/libinput/libinput/-/issues/{issue_id}"

yml = yaml.safe_load(open(ns.recording))

files: List[Tuple[str, Path]] = []

# If we have multiple recordings in a single file, we save the recording into
# each of the directories, with all devices included. This should be cleaned
# up at some point, but for now it'll do.
for device in yml["devices"]:
    id = device["evdev"]["id"]
    vendor, product = id[1:3]
    name = device["evdev"]["name"]
    path = Path(f"{vendor:04X}-{product:04X}-{name.replace('/', '')}")

    path.mkdir(exist_ok=True, parents=True)

    # We may have multiple recordings for a device. Check for duplicates,
    # where the device description is identical, skip adding the new file but
    # prepend the issue to the existing file so we have a reference int
    # the future.
    is_duplicate = False
    for filename in path.glob("*.recording"):
        other = yaml.safe_load(open(filename))
        for other_device in other["devices"]:
            if device["evdev"] == other_device["evdev"]:
                print(f"Identical device found in {filename}")
                content = open(filename).read()
                if issue not in content:
                    with open(filename, "w+") as fd:
                        fd.write(f"# See also: {issue}\n")
                        fd.write(content)
                    files.append((name, filename))
                is_duplicate = True
                break
    if is_duplicate:
        continue

    count = 0
    while True:
        filename = path / f"{issue_id}-{count}.recording"
        if not filename.exists():
            with open(filename, "w+") as fd:
                fd.write(f"# From {issue}\n")
                with open(ns.recording) as infile:
                    fd.write(infile.read())
            files.append((name, filename))
            break
        count += 1

if files:
    commitmsg = f"""Add {' '.join([f[0] for f in files])}

From https://gitlab.freedesktop.org/libinput/libinput/-/issues/{issue}
"""

    args = ["git", "add", *[str(f[1]) for f in files]]
    subprocess.run(args)
    args = ["git", "commit", "--file=-"]
    subprocess.run(args, input=commitmsg, encoding="utf-8")
else:
    print("Nothing to add")
