libinput recordings
===================

A collection of libinput recordings, extracted from the libinput bug tracker.
The primary use-case for these recordings is to have a database of devices, to
search for particular behaviors (e.g. all devices that have `BTN_0`).

**NOTE: recordings are not verified**. This is particularly true for any
files added in the initial batch of commits. Those files were obtained by
parsing all libinput issues and comments and downloading anything that looked
like a recording.

## Adding new recordings

Recordings are saved in a directory named "vendor-product-name", with
recordings themselves being numerically numbered.

Where a recording already exists for a device and the device description does
not differ from the recording, there is no need to add a new recording.

To add a new file to this repository, run the provided script with the
GitLab issue number and the path to the recording:
```
$ ./save-recording.py 123 /path/to/file
```
